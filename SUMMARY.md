# Summary

* [Willkommen](README.md)

## Ergebnisse der OERwerkstatt in Lisum

* [Kompetenzmodelle](Inhalt/Kapitel_1/Kompetenzmodelle_Torben.md)
* [Apps und Tools für die Unterrichtsgestaltung](Inhalt/Kapitel_1/Start_in_die_App_und_Tool_Welt_Julia.md)
* [Sprache Lehren lernen...in einer digitalen Welt](Inhalt/Kapitel_1/sprache_lehren_lernen_digital_Marie.md)
* [LAdigital: Vergleich grundlegender Modelle digitaler Kompetenzen](Inhalt/Kapitel_1/vergleich_kompetenzmodelle_Ines.md)

## Beispiele

* [Text 1](Inhalt/Beispiele/Text_1.md)
* [Text 2](Inhalt/Beispiele/Text_2.md)
  * [Unterkapitel 1](Inhalt/Beispiele/Unterkapitel_1.md)

## Projektinformationen

* [Impressum](Inhalt/impressum.md)
* [Datenschutzerklärung](Inhalt/datenschutzerklärung.md)
