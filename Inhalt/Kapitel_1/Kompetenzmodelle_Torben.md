# Kompetenzmodelle (11.11.2019)

[Link zum Google-Doc-Original](https://docs.google.com/document/d/1Cdntje7CKjpqp-6ZnIj1-3CYLtN9WuvVfNQDYnl7Atw/edit#heading=h.108xxvpq4m6g)

Dieses Material wurde von [Torben Mau](https://twitter.com/TorbenMau) im Rahmen der OERwerkstatt in Lisum erstellt.

### Lizenzhinweis

*Dieser Text steht unter der**[ CC BY 4.0-Lizen*z](http://creativecommons.org/licenses/by/4.0/)* außer besonders gekennzeichnete Inhalte. Der Name des Urhebers soll bei einer Weiterverwendung wie folgt genannt werden: Torben Mau* 

## Einordnung in den Seminarverlauf von #L2D2 (WS 19/20)

<table>
  <tr>
    <td>Kontext:
01. Sitzung "Lehren und Lernen unter den Bedingungen der Digitalisierung und Digitalität - Eine Einführung"
Die Studierenden können verschiedene Positionen im Hinblick auf das Lehren und Lernen unter den Bedingungen der Digitalisierung und Digitalität unterscheiden (Krommer, Stalder, Zierer) 
These: um Medienkompetenz zu unterrichten/vermitteln, muss man selbst medienkompetent sein 
02 Sitzung  “Bildung im Zeitalter der Digitalität: Begriffe, Theorien und Philosophie”
Die Studierenden können verschiedene Strömungen und Denkrichtungen zur Digitalisierung/Digitalität im Kontext von Bildung und Hochschule identifizieren (Allert, Deimann & Clausen, Macgilchrist)
Wie lässt sich digitale Hochschulbildung denken im Zeitalter der Digitalität?
Technologisch
Pädagogisch-didaktisch
Organisatorisch
Politisch    		</td>
  </tr>
  <tr>
    <td>Zielgruppe: Lehramtsstudierende (fächer- und lehramtsübergreifend)</td>
  </tr>
  <tr>
    <td>zeitlicher Bedarf: Selbststudium (ca. 45min) und Präsenzveranstaltung/Seminar (90min)</td>
  </tr>
  <tr>
    <td>Bezüge zu DigCompEdu: 1.3 Reflektierte Praxis, 1.4 Digitale Weiterbildung; 5.3 Aktive Einbindung der Lernenden; 6.2 Digitale Kommunikation und Zusammenarbeit ...</td>
  </tr>
  <tr>
    <td>Lernziele: Die Studierenden 
können zwischen den Kompetenzen von Lehrenden und Lernenden unterscheiden 
können verschiedene Kompetenzmodelle gegenüberstellen und vergleichen 
formulieren Grenzen und Möglichkeiten von Kompetenzmodellen unter Berücksichtigung ihres Vorwissens (u.a. Texte von Macgilchrist) 
übertragen ihre Überlegungen zu Kompetenzmodellen auf ihr eigenes Fach</td>
  </tr>
</table>


## Aufgaben zur Vorbereitung der 4. Sitzung

### Vorbemerkung 

* Die Studierenden sollen je einen Input erarbeiten 

    * Die Dozierenden müssen sicherstellen, dass in der Gruppe der Teilnehmenden jeder Input von ungefähr ähnlich vielen Studierenden erarbeitet und vorbereitet wird 

    * Eine Möglichkeit zur Verteilung der Studierenden auf die verschiedenen Beiträge wäre folgendes Tool: 

        * [Gruppen bilden von André Hermes ist lizenziert unter einer Creative Commons Namensnennung 4.0 International Lizenz.](https://docs.google.com/document/d/1JKejNUI7wjzWLhMG2baUPuLZZh7TYARgQyd2THfguJQ/pub) oder

        * "Random Team Generator"[ https://www.jamestease.co.uk/team-generator/](https://www.jamestease.co.uk/team-generator/)

    * Alternativ können die Dozierenden ein Etherpad/GDoc für die Sitzung mit den jeweiligen Modellen anlegen und dann sollen sich die Studierenden dort nach dem Windhund-Prinzip gleichmäßig verteilt eintragen. (*Hinweis: Dieses Verfahren wurde für die Sitzung am 11.11.19 gewählt*)

### Input 

1. Medienpädagogische Kompetenz

    1. Einleitung aus "Medien und Schule"  (Schaumburg/Prasse 2018, 11-16). [(Leseprobe als PDF bei researchgate.net)](https://www.researchgate.net/profile/Heike_Schaumburg/publication/329371320_Medien_und_Schule/links/5c069e74a6fdcc315f9c0ee9/Medien-und-Schule.pdf) 

2. Bildungspolitische Vorgaben der KMK 

    2. KMK-Strategie. Bildung in der digitalen Welt. Kapitel 2.1.1 Allgemeinbildende Schule (KMK 2016/17, 10-19) [(PDF auf kmk.org)](https://www.kmk.org/fileadmin/Dateien/pdf/PresseUndAktuelles/2018/Digitalstrategie_2017_mit_Weiterbildung.pdf)

    3. Matrix: Kompetenzen in der digitalen Welt (Beschluss der Kultusministerkonferenz, 2016). [(Mindmap auf kmk.org erstellt mit MindManager)](https://www.kmk.org/fileadmin/Dateien/pdf/PresseUndAktuelles/2017/KMK_Kompetenzen_-_Bildung_in_der_digitalen_Welt_Web.html)

3. DigCompEdu

    4. Europäischer Rahmen für die Digitale Kompetenz von Lehrenden (DigCompEdu) [(PDF auf ec.europa.eu)](https://ec.europa.eu/jrc/sites/jrcsh/files/digcompedu_leaflet_de-2018-09-21pdf.pdf) 

    5. Führen Sie browserbasiert die Selbsteinschätzung "DigCompEdu-Checkin” in der [Version für Lehrkräfte an allgemein- oder berufsbildenden Schulen](https://ec.europa.eu/eusurvey/runner/DigCompEdu-S-DE) durch 

4. Dagstuhl-Dreieck  

    6. Dagstuhl-Erklärung: Bildung in der digital vernetzten Welt ([PDF auf gi.de](https://gi.de/fileadmin/GI/Hauptseite/Themen/Dagstuhl-Erkla__rung_2016-03-23.pdf))

    7. Erklärvideo [(auf youtube.com)](https://www.youtube.com/watch?v=_R0p6VSJ47E)  

### Aufgabe 

Wählen Sie einen Inputbereich aus und erarbeiten Sie die jeweiligen Quellen unter der folgenden Fragestellung:  

* Analysieren Sie ein (Kompetenz-)Modell, indem Sie eine SWOT-Analyse durchführen. (Sie können dazu die Tabellenvorlage nutzen)

Speichern Sie Ihr Ergebnis in einem "Google Docs"-Dokument mit Schreibzugriff für Ihre Kommilitonen ab und erstellen Sie einen Kurzlink zum Dokument via [www.kurzelinks.de](www.kurzelinks.de) 

<table>
  <tr>
    <td>Modell</td>
    <td>Ziel- gruppe</td>
    <td>Stärken</td>
    <td>Schwächen</td>
    <td>Chancen</td>
    <td>Risiken</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>


## Ablaufplanung für den 11.11.2019

<table>
  <tr>
    <td>Phase</td>
    <td>Inhalt</td>
    <td>Anmerkung (Komp. Etc.)</td>
  </tr>
  <tr>
    <td>1. Phase 
 (14:00
bis 14:15)</td>
    <td>Organisatorisches (intern)
Zeit für organisatorische Fragen aus den letzten Sitzungen
Teilen Sie die Lerngruppe in 4er-Gruppen auf, in denen jeweils alle vorbereiteten Modelle vertreten sind (falls es nicht aufgeht, lieber 5er-Gruppen mit einem Modell doppelt bilden)</td>
    <td>Begleitend steht ein Dozent als Ansprechpartner im VK- Raum und per Twitter zur Verfügung (Bei Bedarf ein Screen pro Uni)</td>
  </tr>
  <tr>
    <td>2. Phase
 (14:15
bis 15:10) 
</td>
    <td>Aufgabe für die Studierende (intern)
Stellen Sie innerhalb der 4er-Gruppe nacheinander das von Ihnen vorbereitete  Modell vor (max. 20 min)
Erarbeiten Sie auf Basis des Vergleiches und ihrer Vorarbeiten (Hausaufgabe und Texte  vorangegangener Sitzungen) eine gemeinsame SWOT-Analyse über alle vier Modelle (z.B. in einem googleDoc-Dokument oder als Plakat). (ca. 25 min)
Treffen Sie innerhalb Ihrer 4er-Gruppe eine begründete Entscheidung für ein Modell, das  aus der Perspektive Ihres Studiengangs/Ihres Didaktik- bzw. Unterrichtsfaches am tragfähigsten ist.</td>
    <td>Es entstehen pro Standort mehrere SWOT-Analysen in 4er-Gruppen, die im Anschluss in einem Google Docs dokumentiert werden sollen (Link und oder Foto des Plakates hier einfügen)</td>
  </tr>
  <tr>
    <td>3. Phase 
(15:10
bis 15:30)

 </td>
    <td>Sicherung & Veröffentlichung (intern & vernetzt)
Veröffentlichen Sie als 4er-Gruppe ein kurzes Statement (bei Twitter unter #L2D2), warum das von Ihnen gewählte Modell für ihre Gruppe am relevantesten ist (bis 15.15h!)
Sichten Sie die Tweets von den anderen Standorten & Gruppen: Liken Sie als Gruppe ein Statement, das sie gelungen/ unterstützenswert etc. finden und kommentieren Sie mind. ein anderes.</td>
    <td>



Hinweis :
Das eigene Statement kann auch als fotografierter Text getweetet werden!
  
</td>
  </tr>
</table>


	

