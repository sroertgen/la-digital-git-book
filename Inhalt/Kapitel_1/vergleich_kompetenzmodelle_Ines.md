Martin-Luther-Universität  Halle-Wittenberg

Ines Bieler für

Initiative: Lehramt@digital

# LAdigital: Vergleich grundlegender Modelle digitaler Kompetenzen

[Link zum Google-Doc-Original](https://docs.google.com/document/d/1kC_bfgI0CkaRfT_WhBUOH-TRMEIrcqTKtoftZ_tfn8A/edit)

<table>
  <tr>
    <td>Format: ICM, blended learning
</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>zeitlicher Bedarf: </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Lernziele: 
Kennenlernen verschiedener Kompetenzmodelle (TPACK, Dagstuhl-Dreieck, DigCompEdu, digikompP, 4K, KMK-Modell)
Vor- und Nachteile der Modelle unter vergleichender Aufgabenstellung erschließen
kollaborative Produktion eines Erklärvideos zu einem ausgewählten Kompetenzmodell
Evaluation - Relevanz für die eigene Arbeit
</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Kompetenzen:</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>1. Berufliches Engagement
</td>
    <td>1.3 Reflektierte Praxis
Die eigene Praxis hinsichtlich des didaktisch sinnvollen Einsatzes digitaler Medien reflektieren, selbstkritisch beurteilen und aktiv weiterentwickeln.
1.4 Digitale Weiterbildung
Digitale Medien für die berufliche Weiterentwicklung nutzen.
</td>
    <td></td>
  </tr>
  <tr>
    <td>5. Lernerorientierung
</td>
    <td>5.3 Aktive Einbindung der Lernenden
Digitale Medien nutzen, um das aktive und kreative Engagement der Lernenden mit einem Thema zu fördern. Digitale Medien im Rahmen didaktischer Strategien einsetzen, die transversale Fähigkeiten, tiefgründiges Denken und kreativen Ausdruck fördern.
</td>
    <td></td>
  </tr>
  <tr>
    <td>6. Förderung der Digitalen Kompetenz der Lernenden
</td>
    <td>6.1 Informations- und Medienkompetenz
Aktivitäten integrieren, in denen Lernende digitale Medien nutzen, um Informationen und Ressourcen zu finden, zu organisieren, zu verarbeiten, zu analysieren und zu interpretieren, und die Glaubwürdigkeit und Zuverlässigkeit der Informationen und ihrer Quellen kritisch zu bewerten.
6.3 Erstellung digitaler Inhalte
Aktivitäten integrieren, in denen Lernende sich durch digitale Medien ausdrücken und digitale Inhalte in verschiedenen Formaten bearbeiten und erstellen. Lernenden vermitteln, welche Lizenzen und Urheberrechtsbestimmungen für digitale Inhalte gelten und wie man diese berücksichtigt und verwendet
</td>
    <td></td>
  </tr>
  <tr>
    <td>Aufgaben:</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Machen Sie sich mit den og. Kompetenzmodellen vertraut. Nutzen Sie dazu die angegebene Linkliste als Grundlage. </td>
    <td>https://www.gfdb.de/didaktik-tpack-modell/
https://digikomp.at/?id=592
https://ec.europa.eu/jrc/sites/jrcsh/files/digcompedu_leaflet_de-2018-09-21pdf.pdf
https://www.joeran.de/die-4k-skills-was-meint-kreativitaet-kritisches-denken-kollaboration-kommunikation/
https://www.youtube.com/watch?v=_R0p6VSJ47E
</td>
    <td></td>
  </tr>
  <tr>
    <td>Erstellen Sie anhand Ihres Verständnisses eine vergleichende Übersicht zu folgenden Aspekten ...</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Wählen Sie ein Modell aus und erstellen Sie dazu ein Erklärvideo (Technik frei wählbar, max. Dauer 7 min., plattformunabhängig). Laden Sie ihr Erklärvideo in … (je nach Hochschule) hoch.</td>
    <td>Beispiel: 
https://www.youtube.com/watch?v=_R0p6VSJ47E
 
(Ira Diethelm zum Dagstuhl-Dreieck, Erklärvideo - Legetechnik)</td>
    <td></td>
  </tr>
  <tr>
    <td>Bewerten und evaluieren Sie die Arbeiten in Peer Review/Plenum. </td>
    <td></td>
    <td></td>
  </tr>
</table>


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>

Der Name des Urhebers soll bei einer Weiterverwendung wie folgt genannt werden: Martin-Luther-Universität Halle-Wittenberg, Ines Bieler

