# Sprache Lehren lernen...in einer digitalen Welt. (Marie)

[Link zum Google-Doc-Original](https://docs.google.com/document/d/12ejQR_TbwAE0V_LRJEcVyc8CXIHmUFdey04krW8jEuc/edit?ouid=100458124030485512800&usp=docs_home&ths=true)

Willkommen bei der Deutschlehrer:innenbildung. Sie sind Dozierender oder angehende/r Deutschlehrer:in? Sie sind auf der Suche nach Arbeitshilfen zum eigenen Lernen und Lehren mit digitalen Medien - zur methodischen Gestaltung von Schulunterricht und Hochschullehre? Dann sind Sie hier genau richtig und dürfen sich in Ruhe umschauen, inspirieren lassen, entdecken, anwenden.

Nachfolgend aufgelistet gibt es für *Dozierende der Fachdidaktik und Fachwissenschaft Deutsch* Materialien wie Lehr- und Methodenbeispiele sowie Methodensammlungen für die Gestaltung digital unterstützter Lehrveranstaltungen. Zum anderen gibt es für* Studierende des Lehramtsfachs Deutsch* Materialien und Ideen zum Unterrichten u.a. in einer digitalen Welt. Eine allgemeine Sammlung *weiterführender Tools und Ideen* ergänzt das Angebot für Studierende und Dozierende zum Lernen und Ausprobieren im Selbststudium oder die eigene fachliche und methodische Weiterbildung.

Künftig soll diese Sammlung noch weiter wachsen, daher wird es nach und nach noch weitere Beispiele und Materialien geben, die zu einer Lernkarte ausgebaut werden können.

## Für Dozierende

### Lehrmaterialien

* [Elixier](https://www.bildungsserver.de/elixier/elixier2_list.php?feldname2=Systematik%2FFach&feldinhalt2=%22Sprachen+und+Literatur%22&bool2=and&feldname3=Systematik%2FFach&feldinhalt3=%22Deutsch%22&bool3=and&&suche=erweitert&t=Suchen): Suchmaschine des DIPF für qualitätsgesicherte Bildungsmaterialien

* [Methodenspicker](https://d-3.germanistik.uni-halle.de/methodenspicker/): (Digitale) Lehrmethodenkarten des Projekts [D-3] mit Kurzbeschreibungen, Umsetzungstipps und Toolempfehlungen

* [Lernlandschaft](https://ilias.uni-halle.de/goto.php?target=cat_68293&client_id=unihalle): Methodensammlung des Projekts HET LSA für eine heterogenitätssensible Seminargestaltung

* [Methodenpool](http://methodenpool.uni-koeln.de/uebersicht.html): Umfangreiche Methodensammlung der Uni Köln v.a. klassischer, handlungsorientierter Methoden und Techniken

* [Lehrbausteine](https://d-3.germanistik.uni-halle.de/lehrbausteine/): Lehrbeispiele des Projekts [D-3] zur Vermittlung digitaler Kompetenzen

* [Sprachbildung](https://www.sprachen-bilden-chancen.de/index.php/sprachbildende-materialien/informationen-zur-entstehung-und-verwendung-der-aufgaben-und-lehrmaterialien): Materialien zur Sprachbildung der Universität Potsdam

* Webinare zu Lehr- und Methodenimpulsen

    * [Potentiale von Wikis für das kooperative Schreiben](https://www.youtube.com/watch?v=InjHNDNKhWQ)

    * [Prüfen, Messen, Wiegen: Leistungskontrollen und Notenermittlung an (Hoch)Schulen: analog und digital](https://www.youtube.com/watch?v=6Sam81p7NsE)

    * [Social Media im DU](https://www.youtube.com/watch?v=-pqaH77bB3I)

    * [Bildung visuell](https://www.youtube.com/watch?v=_kXBYe1pJ7c)

    * [Lernmodule, Kurse, Studiengang? Wege digitaler Medienbildung an der Universität](https://www.youtube.com/watch?v=GSuPDpFwI9s&list=PLFdAwreFK7ALDpqO9w45tm5dXCAKYJk6M&index=31)

## Für Studierende

### Unterrichtsmaterialien und Unterrichtsideen

* [ZUM-Unterrichten](https://unterrichten.zum.de/wiki/Deutsch)

* [Elixier](https://www.bildungsserver.de/elixier/elixier2_list.php?feldname2=Systematik%2FFach&feldinhalt2=%22Sprachen+und+Literatur%22&bool2=and&feldname3=Systematik%2FFach&feldinhalt3=%22Deutsch%22&bool3=and&&suche=erweitert&t=Suchen): Suchmaschine des DIPF für qualitätsgesicherte Bildungsmaterialien 

* [digi.komp](https://digikomp.at/index.php?id=595&L=0): Sammlung an Unterrichtsbeispielen des Bundes- und Koordinationszentrum eEducation Austria mit Filterfunktion digitaler Kompetenzen

* [Werkzeugkasten](https://www.medien-in-die-schule.de/werkzeugkaesten/)

* [Didaktische Werkstatt - Lehr- und Lernmaterialien](https://home.uni-leipzig.de/fachdidaktik-germ/didaktische-werkstatt/lehr-lernmaterialien/)

### Unterrichtsentwürfe

* [Didaktische Werkstatt - Unterrichtsentwürfe](https://home.uni-leipzig.de/fachdidaktik-germ/didaktische-werkstatt/unterrichtsentwuerfe/)

### Haus- und Abschlussarbeiten

* [Didaktische Werkstatt - Haus- und Abschlussarbeiten](https://home.uni-leipzig.de/fachdidaktik-germ/didaktische-werkstatt/haus-abschlussarbeiten/)

## Weiterführendes

* Digitales Wörterbuch der deutsche Sprache: [https://www.dwds.de/](https://www.dwds.de/)

* Transkriptionssoftware: [https://www.audiotranskription.de/](https://www.audiotranskription.de/)

